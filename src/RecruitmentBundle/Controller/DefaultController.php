<?php

namespace RecruitmentBundle\Controller;

use RecruitmentBundle\Entity\Category;
use RecruitmentBundle\Form\CategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="index_page")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository(Category::class);
        $res = $repository->findAll();

        return $this->render('@Recruitment/Default/index.html.twig', array('category' => $res, 'qq' => '123'));
    }

    /**
     * @param Category $category
     * @Route("/category/{id}", name="show_category")
     */
    public function showAction(Category $category)
    {
        die('dddd');
        var_dump($category);
        $products = $category->getProduct();

        print_r($products);
        /*foreach ($products as $key => $value) {
            echo $value->getName();
        }*/
        return new Response('OK');
    }

    /**
     * @param Request $request
     * @Route("/category/add/", name="add_category")
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(CategoryType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            // ... perform some action, such as saving the task to the database
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($data);
                $entityManager->flush();
            } catch (UniqueConstraintViolationException $ex) {
                $errorCode = '001';
                $errMsg = 'Category insertion error : duplicated data';
            } catch (\Exception $ex) {
                $errorCode = '002';
                $errMsg = 'General error data';
            }

            if (isset($errorCode) && '' != $errorCode) {
                $this->get('logger')->error($errMsg);
                return $this->redirectToRoute('add_category', ['error' => true, 'errorCode' => $errorCode]);
            } else {
                return $this->redirectToRoute('index_page', ['error' => false, 'errorCode' => $errorCode]);
            }
        }

        return $this->render('@Recruitment/Default/category_add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Category $category
     * @Route("/category/delete/{id}", name="delete_category")
     */
    public function deleteAction(Category $category)
    {
        // Remove products
        $connection = $this->getDoctrine()->getConnection();
        $builder = $connection->createQueryBuilder()
            ->delete('product')
            ->where('category_id = :ids')
            ->setParameter(':ids', $category->getId());
        $builder->execute();

        // Remove company
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        // Response
        die('Successful');
    }

    /**
     * @param Request $request
     * @Route("/category/edit/", name="edit_category")
     */
    public function editAction(Request $request)
    {
        // Get data from database
        $categoryObj = $this->getDoctrine()->getRepository('RecruitmentBundle:Category')->find($request->request->get('id'));
        $categoryObj->setCateName($request->request->get('name'));
        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categoryObj);
            $em->flush();
        } catch (\Exception $ex) {
            print_r($ex->getTrace());
            die();
        }

        // Response
        die('Successful');
    }
}
